# demo_keras - A very simple demo to use keras within ROS

Thi simple demo takes an image stream (configured via the ROS parameter server). processes it with keras and publishes severa output streams.

## Node demo_keras

### Publish

*   /object_detected
*   /object_detected_propability
*   /object_detected_mqtt


### Parameters

## Simple Commandline usage

This is going to take a lot of terminals, but it will work.
It is important to start from the 2018tmls working directory/

### Start the ROS core process

    $ source tmls.env
    $ roscore

###  Start the camera

In a new terminal, start a USB camera through OpenCV

    $ source tmls.env
    $ rosparam set cv_camera/device_id 0
    $ rosrun cv_camera cv_camera_node

Or the Raspicam

    tbd

To check the image, you can use rqt to view the image stream.

### Start classification node

In a new terminal, start the classifier node:

    $ source tmls.env
    $ rosrun

To check for classification output, you can first check if there is a topic stream from the detection node:

    $ rostopic list
    /cv_camera/camera_info
    /cv_camera/image_raw
    /cv_camera/image_raw/compressed
    /cv_camera/image_raw/compressed/parameter_descriptions
    /cv_camera/image_raw/compressed/parameter_updates
    /cv_camera/image_raw/compressedDepth
    /cv_camera/image_raw/compressedDepth/parameter_descriptions
    /cv_camera/image_raw/compressedDepth/parameter_updates
    /cv_camera/image_raw/theora
    /cv_camera/image_raw/theora/parameter_descriptions
    /cv_camera/image_raw/theora/parameter_updates
    /object_detected
    /object_detected_mqtt
    /object_detected_probability
    /rosout
    /rosout_agg

Now we can stream the detected objetcs:

    $ rostopic echo /object_detected
    data: "mosquito_net"
    ---
    data: "mosquito_net"
    ---
    data: "mosquito_net"

