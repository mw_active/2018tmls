#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple object detection with Keras in ROS

This module demonstrates how to use machine learning in combination with the Robot Operating System ROS.
It is written to be part of the ROS build system catkin.

Most of this code is based on Matthew's `Visual Object Recognition Blogpost`_.

.. _Visual Object Recognition Blogpost: http://projectsfromtech.blogspot.com/2017/10/visual-object-recognition-in-ros-using.html
"""

# Python 3 compatibility
from __future__ import print_function

# Import Scientific and Vision Libraries
import cv2  # OpenCV
import scipy as sp # I prefer scipy instead of numpy
import pylab as plt
import sys
import platform
import json

# Impport ROS base packages
import rospy
import roslib

# Import the messages for topic broadcasts
from std_msgs.msg import String
from std_msgs.msg import Float32
from sensor_msgs.msg import Image
from aws_mqtt_bridge.msg import MQTT_publish

# Import OpenCV image handling within ROS.
from cv_bridge import CvBridge, CvBridgeError

# Import Keras and Tensorflow
import tensorflow as tf
from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50, preprocess_input, decode_predictions


class DemoKeras(object):
  """ Class to demonstrate ML on ROS with Keras
  """
  def __init__(self, topic="test/keras_object"):
    """ Initializes the class to be ready for ROS

    Args:
      topic (str): MQTT topic to publsih to.

    """
    print("Initializing intsance of DemoKeras class")
    self._target_size = (224, 224) # Rescale image size to this value
    # Prepare Messages
    self._msg_string = String()
    self._msg_float  = Float32()
    self._msg_mqtt = MQTT_publish()
    self._mqtt_shadow = {
      "state":{
        "reported":{
          "ros_status":"ROS is running",
          "keras_object":"test",
          "keras_prop":"test"
        }
      }
    }
    self._mqtt_topic = topic

    # Prepare Model
    self._model, self._graph = self._initialize_keras()
    # Prepare ROS publishers
    self._pub, self._bridge = self._initialize_ros()

  def _initialize_keras(self):
    """ Initialize Keras

    Returns:
      model, graph: The ML model and tensorflow compute graph.
    """
    model = ResNet50(weights='imagenet')
    model._make_predict_function()
    graph = tf.get_default_graph()

    return model, graph

  def _initialize_ros(self):
    """Initialize the ROS node

    Returns:
      pub(dict), bridge: Dictionary of publishers and OpenCV bridge
    """
    rospy.init_node('demo_keras', anonymous=True)
    # Prepare message publisher
    pub = {}
    pub["object"] = rospy.Publisher('object_detected', String, queue_size = 1)
    pub["propability"] = rospy.Publisher('object_detected_probability', Float32, queue_size = 1)
    pub["mqtt"] = rospy.Publisher('object_detected_mqtt',MQTT_publish, queue_size = 1 )
    pub["image"] = rospy.Publisher('keras_image',Image, queue_size = 1)
    # Create OpenCV for image resizing
    bridge = CvBridge()

    return pub, bridge

  def preprocess_image(self,image_msg):
    """Convert image message

    """
    # Use the opencv bridge to convert the ros image messsage into OpenCV object.
    image = self._bridge.imgmsg_to_cv2(image_msg, desired_encoding="passthrough")
    # Resize image
    image = cv2.resize(image, self._target_size)
    # Broadcast resized image into topic
    self._pub['image'].publish(
        self._bridge.cv2_to_imgmsg(image,encoding='bgr8')
    )
    # Recast image to an array
    image = sp.asarray(image)
    # Make it tensorflow compatible
    image = sp.expand_dims(image, axis=0)
    # Convert to float64
    image = image.astype(float)
    # Apply data regularization
    image = preprocess_input(image)

    return image

  def ros_callback(self,image_msg):
    """ Ros subscriber callback function

    """
    # Convert image message into numpy array
    sp_image = self.preprocess_image(image_msg)

    # Start ML processing pipeline
    with self._graph.as_default():
      # Classify the image
       pred = self._model.predict(sp_image)
       # decode returns a list  of tuples [(class,description,probability),...
       pred_string = decode_predictions(pred, top=1)[0]   # Decode top 1 predictions
       self._msg_string.data = pred_string[0][1]
       self._msg_float.data = float(pred_string[0][2])
       self._pub["object"].publish(self._msg_string)
       self._pub["propability"].publish(self._msg_float)

       self._msg_mqtt.topic = self._mqtt_topic
       self._mqtt_shadow["state"]["reported"]["keras_object"]=str(pred_string[0][1])
       self._mqtt_shadow["state"]["reported"]["keras_prob"]=str(pred_string[0][2])
       self._msg_mqtt.payload = json.dumps(self._mqtt_shadow)
       self._pub["mqtt"].publish(self._msg_mqtt)



def main(argv):
  '''Main function.

  This function creates a classifier object, subscribes to the camera
  topic and the spins until ros is terminated.
  '''

  topic = '$aws/things/' + platform.node() + '/shadow/update'
  myClassifier = DemoKeras(topic=topic)
  rospy.Subscriber("cv_camera/image_raw", Image,
                    myClassifier.ros_callback,
                    queue_size = 1, buff_size = 16777216
  )

  while not rospy.is_shutdown():
    rospy.spin()


if __name__ == '__main__':
  main(sys.argv)




