# Machine Learning and Robotics - ROS, Raspberry Pi, IoT

A Workshop hosted by MistyWest and Kindred at the [Toronto Machine Learning Series (2018 TMLS)](https://torontomachinelearning.com/)

## Workshop Agenda

TODO


## Prerequisites

Anyone who wants to try the examples of the workshop on their own computers will need the following.

We will also have a number of Raspberry Pis at the workshop.

### Computer:

* Laptop with Linux, ideally Ubuntu 16.04
* USB camera

## Repository Structure

| Directory | Purpose |
|-----------|---------|
| aws       | CloudFormation template to build a AWS IoT instance |
| catkin_ws      | Source code for the ROS part of the demo |
| -> src -> ml -> demo_keras | Really simple keras demo     |
| thirdparty     | Third party libraries |
| -> aws_mqtt_bridge | The ROS to AWS package (git submodule) |

### Rpository Checkout:

    $ git clone git@bitbucket.org:mw_active/2018tmls.git
    $ git submodule init
    $ git submodule update

### Fist steps:

####  Install the ROS, Python and other dependencies

```
$ cd 2018tmls
$ sudo bin/bin/tmls-install-required.sh
```

####  Lets see if ROS is alive: Play with the turtles

```
$ source tmls.env
$ roslaunch turtle_tf2 turtle_tf2_demo.launch
# For some more entertainment, try
$ rviz turtle_rviz_groovy.rviz
```


####  Let's compile our code

```
$ source tmls.env
$ cd catkin_ws
$ catkin_make
Base path: /home/aputz_l/MistyWest/OSC/2018tmls/catkin_ws
Source space: /home/aputz_l/MistyWest/OSC/2018tmls/catkin_ws/src
Build space: /home/aputz_l/MistyWest/OSC/2018tmls/catkin_ws/build
Devel space: /home/aputz_l/MistyWest/OSC/2018tmls/catkin_ws/devel
Install space: /home/aputz_l/MistyWest/OSC/2018tmls/catkin_ws/install
####
#### Running command: "make cmake_check_build_system" in "/home/aputz_l/MistyWest/OSC/2018tmls/catkin_ws/build"
####
####
#### Running command: "make -j8 -l8" in "/home/aputz_l/MistyWest/OSC/2018tmls/catkin_ws/build"
####
[  0%] Built target std_msgs_generate_messages_py
[ 16%] Built target gmock
[ 16%] Built target std_msgs_generate_messages_cpp
[ 16%] Built target std_msgs_generate_messages_nodejs
[ 27%] Built target gtest
[ 50%] Built target gmock_main
[ 50%] Built target std_msgs_generate_messages_eus
[ 50%] Built target std_msgs_generate_messages_lisp
[ 61%] Built target gtest_main
[ 61%] Built target _aws_mqtt_bridge_generate_messages_check_deps_MQTT_publish
[ 72%] Built target aws_mqtt_bridge_generate_messages_eus
[ 77%] Built target aws_mqtt_bridge_generate_messages_cpp
[ 88%] Built target aws_mqtt_bridge_generate_messages_py
[ 94%] Built target aws_mqtt_bridge_generate_messages_nodejs
[100%] Built target aws_mqtt_bridge_generate_messages_lisp
[100%] Built target aws_mqtt_bridge_generate_messages
```