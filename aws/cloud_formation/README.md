# CloudFormation Template For Basic AWS IoT functionality


CloudFormation allows for the programmatic deployment of AWS resources within an AWS account.

The attached template will generate a few simple resources:

* An S3 bucket
* A few policies and roles
* A custom resource to write some configuration files.

