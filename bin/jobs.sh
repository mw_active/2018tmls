#!/bin/bash

python2.7 ../thirdparty/aws-iot-device-sdk-python/samples/jobs/jobsSample.py -e a3g1ym8g0kwvjd-ats.iot.us-west-2.amazonaws.com -r ~/.aws_client/x509/AmazonRootCA1.pem -c ~/.aws_client/x509/9ec8c18431-certificate.pem.crt -k ~/.aws_client/x509/9ec8c18431-private.pem.key -n $(hostname)

